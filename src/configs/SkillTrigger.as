package configs
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class SkillTrigger
	{
		
		public static const trigger_selfEnter:int = 1;
		public static const trigger_selfTapped:int = 2;
		public static const trigger_selfDead:int = 3;
		public static const trigger_selfAttack:int = 4;
		
		public static const trigger_selfAttacked:int = 5;
		public static const trigger_selfRongHe:int = 6;
		public static const trigger_selfKill:int = 7;
		
		public static const trigger_selfHpInRangePercent:int = 8;
		public static const trigger_selfHpInRange:int = 9;
		
		public static const trigger_selfHasMaleArmMove:int = 10;
		public static const trigger_selfHasFemaleArmMove:int = 11;
		public static const trigger_selfHasBeastArmMove:int = 12;
		public static const trigger_selfHasElementArmMove:int = 13;
		public static const trigger_selfHasMachineArmMove:int = 14;
		
		public static const trigger_enemyHasMaleArmMove:int = 15;
		public static const trigger_enemyHasFemaleArmMove:int = 16;
		public static const trigger_enemyHasBeastArmMove:int = 17;
		public static const trigger_enemyHasElementArmMove:int = 18;
		public static const trigger_enemyHasMachineArmMove:int = 19;
		
		public static const trigger_selfWeiguoEnter:int = 20;
		public static const trigger_selfShuguoEnter:int = 21;
		public static const trigger_selfWuguoEnter:int = 22;
		public static const trigger_selfQunguoEnter:int = 23;
		public static const trigger_selfJinChengEnter:int = 24;
		public static const trigger_selfYuanChengEnter:int = 25;
		public static const trigger_aoyiChuFa:int = 26;
		
		public static const trigger_selfSideEnter:int = 27;		//我方有单位补进
		public static const trigger_enemyEnter:int = 28;
		
		public static const trigger_selfHeroEnterWithOid:int = 29;			//我方某个武将补进
		
		public static const trigger_selfWujiangEnger:int = 30;
		
		public static var skillTrigger:ArrayCollection = new ArrayCollection(
			[
				{"type":SkillTrigger.trigger_selfEnter, "desc":"自己入场"},
				{"type":SkillTrigger.trigger_selfTapped, "desc":"自己点掉"},
				{"type":SkillTrigger.trigger_selfDead, "desc":"自己死亡"},
				{"type":SkillTrigger.trigger_selfAttack, "desc":"自己攻击"},
				{"type":SkillTrigger.trigger_selfAttacked, "desc":"自己被攻击"},
				{"type":SkillTrigger.trigger_selfRongHe, "desc":"自己融合"},
				{"type":SkillTrigger.trigger_selfKill, "desc":"自己杀死敌人"},
				{"type":SkillTrigger.trigger_selfHpInRangePercent, "desc":"自己生命值在一定范围(百分比)"},
				{"type":SkillTrigger.trigger_selfHpInRange, "desc":"自己生命值在一定范围"},
				{"type":SkillTrigger.trigger_selfHasMaleArmMove, "desc":"我方有男性单位补进"},
				{"type":SkillTrigger.trigger_selfHasFemaleArmMove, "desc":"我方有女性单位补进"},
				{"type":SkillTrigger.trigger_selfHasBeastArmMove, "desc":"我方有野兽单位补进"},
				{"type":SkillTrigger.trigger_selfHasElementArmMove, "desc":"我方有元素单位补进"},
				{"type":SkillTrigger.trigger_selfHasMachineArmMove, "desc":"我方有机械单位补进"},
				
				{"type":SkillTrigger.trigger_enemyHasMaleArmMove, "desc":"敌方有男性单位补进"},
				{"type":SkillTrigger.trigger_enemyHasFemaleArmMove, "desc":"敌方有女性单位补进"},
				{"type":SkillTrigger.trigger_enemyHasBeastArmMove, "desc":"敌方有野兽单位补进"},
				{"type":SkillTrigger.trigger_enemyHasElementArmMove, "desc":"敌方有元素单位补进"},
				{"type":SkillTrigger.trigger_enemyHasMachineArmMove, "desc":"敌方有机械单位补进"},
				
				{"type":SkillTrigger.trigger_selfWeiguoEnter, "desc":"我方有魏国单位补进"},
				{"type":SkillTrigger.trigger_selfShuguoEnter, "desc":"我方有蜀国单位补进"},
				{"type":SkillTrigger.trigger_selfWuguoEnter, "desc":"我方有吴国单位补进"},
				{"type":SkillTrigger.trigger_selfQunguoEnter, "desc":"我方有群雄单位补进"},
				{"type":SkillTrigger.trigger_selfJinChengEnter, "desc":"我方有近程单位补进"},
				{"type":SkillTrigger.trigger_selfYuanChengEnter, "desc":"我方有远程单位补进"},
				
				{"type":SkillTrigger.trigger_aoyiChuFa, "desc":"奥义触发"},
				{"type":SkillTrigger.trigger_selfSideEnter, "desc":"我方有任意单位入场"},
				{"type":SkillTrigger.trigger_enemyEnter, "desc":"敌方有任意单位入场"},
				{"type":SkillTrigger.trigger_selfHeroEnterWithOid, "desc":"我方某个英雄入场触发"},
				{"type":SkillTrigger.trigger_selfWujiangEnger, "desc":"武将上场技触发"}
			]
		);
		
		public static function getTriggerIndex(trigger:int):int
		{
			for(var index:int=0; index<skillTrigger.length; index++)
			{
				var triggerObj:Object = skillTrigger.getItemAt(index);
				if(triggerObj["type"] == trigger)
					return index;
			}
			return -1;
		}
		
		public function SkillTrigger()
		{
		}
	}
}