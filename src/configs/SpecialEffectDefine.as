package configs
{
	import mx.collections.ArrayCollection;

	public class SpecialEffectDefine
	{
		
		public static const none:int = 0;
		public static const changeHpMaxXiaPer:int = 1;
		public static const changeHpMaxValue:int = 2;
		public static const dirChangeHpPer:int = 3;
		public static const dirChangeHpValue:int = 4;
		public static const changeHpInRoundPer:int = 5;
		public static const changeHpInRoundValue:int = 6;
		public static const changeDamagePer:int = 7;
		public static const changeDamageValue:int = 8;
		public static const changeDamgeFromTarget:int = 9;
		public static const changeDamgeFromId:int = 10;
		
		public static const changeDamageInRoundPer:int = 11;
		public static const changeDamageInRoundValue:int = 12;
		
		public static const changeHpWhenHurtPer:int = 13;
		public static const changeHpWhenHurtValue:int = 14;
		
		public static const XiXue:int = 15;
		public static const NLianJi:int = 16;
		public static const fujiGongjiPer:int = 17;
		public static const burn:int = 18;
		public static const frozen:int = 19;
		public static const proFanJiWhenAtked:int = 20;
		public static const fantanWhenAtkedDamagePer:int = 21;
		public static const gainCallPoint:int = 22;
		public static const gainShenShouPoint:int = 23;			//获得神兽能量点
		public static const gainBattleCards:int = 24;
		public static const callUnitOfSomePoints:int = 25;
		public static const callSomeUnitsById:int = 26;
		
		public static const ShangHaiShuChuZengJia:int = 27;
		public static const shanghaiXiShou:int = 28;
		public static const jiNengChuFa:int = 29;
		public static const baohuqiang:int = 30;
		public static const AoYi:int = 31;
		
		public static const bingLiBuChong:int = 32;
		public static const jiefeng:int = 33;
		
		public static const changeDamgeFromTargetPer:int = 34;
		public static const changeDamgeFromIdPer:int = 35;
		
		public static const randomCard:int = 36;
		
		public static const onlyMiaoSha:int = 37;
		public static const zidongHuiXue:int = 38;
		public static const mianyiXiaogguo:int = 39;
		public static const onlyRanShao:int = 40;
		public static const budongNHuiHe:int = 41;
		public static const tishengAttack:int = 42;
		public static const onlyWuJiang:int = 43;
		public static const defendDebuff_Ice:int = 44;
		public static const defendDebuff_Fire:int = 45;
		
		public static var allWaveBuffs:ArrayCollection = new ArrayCollection([
			{"type":SpecialEffectDefine.none,"desc":"无"},
			{"type":SpecialEffectDefine.onlyMiaoSha,"desc":"只能秒杀"},
			{"type":SpecialEffectDefine.zidongHuiXue,"desc":"每攻击N次回复M%的血"},
			{"type":SpecialEffectDefine.mianyiXiaogguo,"desc":"免疫所有效果"},
			{"type":SpecialEffectDefine.onlyRanShao,"desc":"只受燃烧伤害"},
			{"type":SpecialEffectDefine.budongNHuiHe,"desc":"N回合不动"},
			{"type":SpecialEffectDefine.tishengAttack,"desc":"每攻击N回合提升M%攻击"},
			{"type":SpecialEffectDefine.onlyWuJiang,"desc":"只受武将攻击"},
		]);
		
		public static function getWavebuffById(idType:int):int{
			for(var i:int = 0;i < allWaveBuffs.length;i++)
			{
				var singleInfo:Object = allWaveBuffs[i];
				if(singleInfo["type"] == idType)
				{
					return i;
				}
			}
			return -1;
		}
		
		[Bindable]
		public static var bufflist:ArrayCollection = new ArrayCollection(
			[
				{"type":SpecialEffectDefine.changeHpMaxXiaPer, "desc":"增/减 N% 的生命上限"},
				{"type":SpecialEffectDefine.changeHpMaxValue, "desc":"增/减 N点 的生命上限"},
				{"type":SpecialEffectDefine.dirChangeHpPer, "desc":"增/减 N% 的HP"},
				{"type":SpecialEffectDefine.dirChangeHpValue, "desc":"增/减 N点 的HP"},
				{"type":SpecialEffectDefine.changeHpInRoundPer, "desc":"每回合 增/减 N% 的HP 永久"},
				{"type":SpecialEffectDefine.changeHpInRoundValue, "desc":"每回合 增/减 N点 的HP 永久"},
				{"type":SpecialEffectDefine.changeDamagePer, "desc":"增/减 N% 的攻击 临时"},
				{"type":SpecialEffectDefine.changeDamageValue, "desc":"增/减 N点 的攻击 临时"},
				{"type":SpecialEffectDefine.changeDamgeFromTarget, "desc":"场上每有一个（2),使(1)增/减N点攻击 临时"},
				{"type":SpecialEffectDefine.changeDamgeFromId, "desc":"场上每有一个id的(2),使(1)增/减N点攻击 临时"},
				{"type":SpecialEffectDefine.changeDamageInRoundPer, "desc":"每回合 增/减 N% 的攻击 永久 会叠加 慎用"},
				{"type":SpecialEffectDefine.changeDamageInRoundValue, "desc":"每回合 增/减 N点 的攻击 永久 会叠加 慎用"},
				{"type":SpecialEffectDefine.changeHpWhenHurtPer, "desc":"每回合 受到的伤害 增/减 N% 的伤害"},
				{"type":SpecialEffectDefine.changeHpWhenHurtValue, "desc":"每回合 受到的伤害时 增/减 N点 的伤害"},
				{"type":SpecialEffectDefine.XiXue, "desc":"每回合 对敌人造成伤害的 N% 转化成自己HP"},
				{"type":SpecialEffectDefine.NLianJi, "desc":"攻击 变为 N 连击"},
				{"type":SpecialEffectDefine.fujiGongjiPer, "desc":"进行附加攻击 造成 自己原始伤害的 N%"},
				{"type":SpecialEffectDefine.burn, "desc":"燃烧"},					//中毒
				{"type":SpecialEffectDefine.frozen, "desc":"冻结"},					//眩晕
				{"type":SpecialEffectDefine.proFanJiWhenAtked, "desc":"每次受到攻击 有 N% 概率 反击对手"},
				{"type":SpecialEffectDefine.fantanWhenAtkedDamagePer, "desc":"受到攻击时 使自身受到伤害的 N% 反弹给攻击者"},
				{"type":SpecialEffectDefine.gainCallPoint, "desc":"获得 N 召唤点"},
				{"type":SpecialEffectDefine.gainShenShouPoint, "desc":"获得 N 神兽点"},
				{"type":SpecialEffectDefine.gainBattleCards, "desc":"获得 N 张卡牌"},
				{"type":SpecialEffectDefine.callUnitOfSomePoints,"desc":"召唤 N 召唤点的单位"},
				{"type":SpecialEffectDefine.callSomeUnitsById,"desc":"召唤 N 个（目标1）"},
				
				{"type":SpecialEffectDefine.ShangHaiShuChuZengJia,"desc":"伤害输出增加"},
				{"type":SpecialEffectDefine.shanghaiXiShou,"desc":"伤害吸收"},
				{"type":SpecialEffectDefine.jiNengChuFa,"desc":"技能触发"},
				{"type":SpecialEffectDefine.baohuqiang,"desc":"保护墙"},
				{"type":SpecialEffectDefine.AoYi,"desc":"奥义"},
				
				{"type":SpecialEffectDefine.bingLiBuChong,"desc":"兵力补充"},
				{"type":SpecialEffectDefine.jiefeng,"desc":"解封"},
				{"type":SpecialEffectDefine.changeDamgeFromTargetPer,"desc":"场上每有一个（2),使(1)增/减N%攻击"},
				{"type":SpecialEffectDefine.changeDamgeFromIdPer,"desc":"解场上每有一个id（2),使(1)增/减%点攻击"},
				{"type":SpecialEffectDefine.randomCard,"desc":"随机卡牌"},
				{"type":SpecialEffectDefine.onlyMiaoSha,"desc":"只能秒杀"},
				{"type":SpecialEffectDefine.zidongHuiXue,"desc":"每攻击N次回复M%的血"},
				{"type":SpecialEffectDefine.mianyiXiaogguo,"desc":"免疫所有效果"},
				{"type":SpecialEffectDefine.onlyRanShao,"desc":"只受燃烧伤害"},
				{"type":SpecialEffectDefine.budongNHuiHe,"desc":"N回合不动"},
				{"type":SpecialEffectDefine.tishengAttack,"desc":"每攻击N回合提升M%攻击"},
				{"type":SpecialEffectDefine.onlyWuJiang,"desc":"只受武将攻击"},
				{"type":SpecialEffectDefine.defendDebuff_Ice,"desc":"负面状态抗性_冰冻"},
				{"type":SpecialEffectDefine.defendDebuff_Fire,"desc":"负面状态抗性_燃烧"}
			]
		);
		
		
		
		/**
		 * 获得buff类型
		 */
		public static function getBufftypeIndex(buffeid:int):int
		{
			for(var index:int=0; index< SpecialEffectDefine.bufflist.length; index++)
			{
				var buffObj:Object = SpecialEffectDefine.bufflist.getItemAt(index);
				if(buffObj["type"] == buffeid)
					return index;
			}
			
			return -1;
		}
		
		public function SpecialEffectDefine()
		{
		}
	}
}