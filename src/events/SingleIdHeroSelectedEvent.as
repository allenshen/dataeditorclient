package events
{
	import defines.HeroInfo;
	
	import flash.events.Event;
	
	public class SingleIdHeroSelectedEvent extends Event
	{
		
		public static var EventTag:String = "heroIdSelectTag";
		
		public var selectheroInfo:HeroInfo;
		
		public function SingleIdHeroSelectedEvent(type:String,heroId:HeroInfo)
		{
			super(type, false, false);
			this.selectheroInfo = heroId;
		}
	}
}