package events
{
	import defines.ArmInfoObj;
	
	import flash.events.Event;
	
	public class SingleIdArmSelectedEvent extends Event
	{
		public static const singleIdArmSelected:String = "singleIdArmSelected";
		public static const singleIdArmSelectedForUnlock = "singleArmSelectedForUnlock";		
		public var armDataInfo:ArmInfoObj;
		
		public function SingleIdArmSelectedEvent(type:String,armInfo:ArmInfoObj)
		{
			this.armDataInfo = armInfo;
			super(type);
		}
	}
}