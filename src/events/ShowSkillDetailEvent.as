package events
{
	import flash.events.Event;
	
	public class ShowSkillDetailEvent extends Event
	{
		
		public static const showSkillDetailevent:String = "showSkillDetailEvent";
		public var contentId:int = 0;
		
		public function ShowSkillDetailEvent(type:String, contentId:int)
		{
			super(type);
			this.contentId = contentId;
		}
	}
}