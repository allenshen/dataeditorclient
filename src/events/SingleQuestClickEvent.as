package events
{
	import flash.events.Event;
	
	public class SingleQuestClickEvent extends Event
	{
		
		public static var QuestClickEvent:String = "questClickEvent";

		public var contentId:int = 0;
		
		public function SingleQuestClickEvent(type:String, contentId:int)
		{
			this.contentId = contentId;
			super(type);
		}
		
	}
}