package events
{
	import flash.events.Event;

	public class BuffEditEvent extends Event
	{
		public var data:* = null;
		
		public static const BuffDeleted:String = "BuffDeleted";
		
		public function BuffEditEvent(type:String, data:* = null)
		{
			this.data = data;
			super(type, true, true);
		}
	}
}