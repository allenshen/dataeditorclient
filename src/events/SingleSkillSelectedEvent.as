package events
{
	import defines.Skill;
	
	import flash.events.Event;
	
	public class SingleSkillSelectedEvent extends Event
	{
		
		public static const skillSlelectedEvent:String = "singleSkillSelectedEvent";
		public var contentSkillInfo:Skill;
		
		public function SingleSkillSelectedEvent(type:String,skillInfo:Skill)
		{
			this.contentSkillInfo = skillInfo;;
			super(type);
		}
	}
}