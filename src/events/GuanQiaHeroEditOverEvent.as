package events
{
	import defines.HeroInfo;
	
	import flash.events.Event;
	
	public class GuanQiaHeroEditOverEvent extends Event
	{
		
		public static const singleHeroEditOverEvent:String = "singleHeroOverEvent";
		
		public var sourceHeroInfo:HeroInfo;
		
		public function GuanQiaHeroEditOverEvent(type:String,heroInfo:HeroInfo)
		{
			super(type);
			this.sourceHeroInfo = heroInfo;
		}
	}
}