package events
{
	import defines.ArmInfoObj;
	
	import flash.events.Event;
	
	public class ArmForAppearTypeParamEvent extends Event
	{
		
		public var armInfo:ArmInfoObj;
		public static const singleArmForAppearParam:String = "singleArmForAppearParam";
		
		public function ArmForAppearTypeParamEvent(type:String,armInfo:ArmInfoObj)
		{
			this.armInfo = armInfo;
			super(type);
		}
	}
}