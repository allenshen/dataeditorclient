package events
{
	import defines.ArmInfoObj;
	
	import flash.events.Event;
	
	public class DanboUnitEditOverEvent extends Event
	{
		
		public static const singleUnitEditDone:String = "singleUnitEditDoneEvent";
		
		public var contentArmInfo:ArmInfoObj;
		
		public function DanboUnitEditOverEvent(type:String, armInfo:ArmInfoObj)
		{
			super(type);
			this.contentArmInfo = armInfo;
		}
	}
}