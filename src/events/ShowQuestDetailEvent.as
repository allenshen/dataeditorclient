package events
{
	import flash.events.Event;
	
	public class ShowQuestDetailEvent extends Event
	{
		
		public static const ShowQuestDetailEventTag:String = "showQuestDetailEvent";
		public var contentId:int = 0;
		
		public function ShowQuestDetailEvent(type:String, contentId:int)
		{
			super(type);
			this.contentId = contentId;
		}
	}
}