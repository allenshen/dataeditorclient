package editors.chapter
{
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import mx.core.UIComponent;
	
	import spark.components.BorderContainer;
	import spark.components.Group;
	
	public class SinglePathPoint extends UIComponent
	{
		
		public var hostGuanQiaId:int = 0;
		
		private var _status:int = 0;
		
		public var type:int = 0;
			
		public function SinglePathPoint()
		{
			super();
		}
		
		public function get status():int
		{
			return _status;
		}

		public function set status(value:int):void
		{
			_status = value;
			if(type == 0)
			{
				if(this.status == 0)
				{
					this.graphics.clear();
					this.graphics.beginFill(0x0000ff,1);
					this.graphics.drawCircle(-12,-14,6);
					this.graphics.endFill();
				}
				else
				{
					this.graphics.clear();
					this.graphics.beginFill(0xff0000,1);
					this.graphics.drawCircle(-12,-14,6);
					this.graphics.endFill();
				}
			}
			else if(type == 1) 
			{
				if(this.status == 0)
				{
					this.graphics.clear();
					this.graphics.beginFill(0x0000ff,1);
					this.graphics.drawCircle(-4,-15,10);
					this.graphics.endFill();
				}
				else
				{
					this.graphics.clear();
					this.graphics.beginFill(0xff0000,1);
					this.graphics.drawCircle(-4,-15,10);
					this.graphics.endFill();
				}
			}
			else if(type == 2) 
			{
				if(this.status == 0)
				{
					this.graphics.clear();
					this.graphics.beginFill(0x00ffff,1);
					this.graphics.drawCircle(-4,-15,10);
					this.graphics.endFill();
				}
				else
				{
					this.graphics.clear();
					this.graphics.beginFill(0xffff00,1);
					this.graphics.drawCircle(-4,-15,10);
					this.graphics.endFill();
				}
			}
		}

	}
}