package com.sprite_sheet
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	
	public class SpriteSheetFrame extends Sprite 
	{
		protected var mWidth: int;
		protected var mHeight: int;
		protected var _mBitmap: Bitmap;
		
		public function SpriteSheetFrame(bmp: Bitmap, width: int, height: int)
		{
			mWidth = width;
			mHeight = height;
			
			_mBitmap = bmp;
			addChild(mBitmap);
		}
		
		public function get mBitmap():Bitmap
		{
			return _mBitmap;
		}

		public function getWidth(): int
		{
			return mWidth;
		}
		
		public function getHeight(): int
		{
			return mHeight;
		}
	}
}
