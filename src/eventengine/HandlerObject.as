package eventengine
{
	public class HandlerObject
	{
		
		public var handler:Function;

		public var params:* = null;
		private var _hasParma:Boolean = false;
		
		public function HandlerObject(handler:Function,params:* = null)
		{
			this.handler = handler;
			this.params = params; 
			if(this.params != null)
				this._hasParma = true;
		}
		
		public function get hasParma():Boolean
		{
			return _hasParma;
		}
	}
}