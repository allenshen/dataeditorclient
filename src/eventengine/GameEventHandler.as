package  eventengine
{
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;

	/**
	 *  获得全局唯一的用于某个用途的事件收发器，例如引导事件，任务事件等等
	 * @author SDD
	 * 
	 */
	public class GameEventHandler
	{
		
		private static var eventListerPool:Object={};
		
		public function GameEventHandler()
		{
		}
		
		private static function getHandler(eventType:String):CommonEventController
		{
			if(!eventListerPool[eventType])
				eventListerPool[eventType] = new CommonEventController;
			return eventListerPool[eventType] as CommonEventController;
		}
		
		public static function dispatchGameEvent(id:String,event:*):void
		{
			var targetCommonController:CommonEventController = getHandler(id) as CommonEventController;
			if(targetCommonController)
				targetCommonController.dispatchEvent(event);
		}
		
		public static function update(id:String,item:CommonEventController):void
		{
			eventListerPool[id] = item;
		}
		
		public static function addListener(id:String,eventType:String,listener:*,params:* = null):void
		{
			(getHandler(id) as CommonEventController).regEventHandler(eventType,listener,params);
		}
		
		public static function removeListener(id:String,eventType:String,handler:* = null):void
		{
			(getHandler(id) as CommonEventController).disRegEventHandler(eventType,handler);
		}
		
		public static function getLisnersCount(id:String,eventType:String):int
		{
			var count:int = 0;
			var targetController:CommonEventController = getHandler(id);
			targetController.getListenerCountOfType(eventType);
			return count;
		}
		
		public static function hasListener(id:String,eventType:String):Boolean
		{
			var retValue:Boolean = false;
			var targetCommonController:CommonEventController = getHandler(id) as CommonEventController;
			if(targetCommonController)
			{
				if(targetCommonController.m_executers.hasOwnProperty(eventType))
				{
					var tempDictionary:Dictionary = targetCommonController.m_executers[eventType];
					if(tempDictionary)
					{
						for(var singleHandler:* in tempDictionary)
						{
							retValue = true;
							break;
						}
					}
				}
			}
			return retValue;
		}
		
		/**
		 * 将某个id的 CommonEventController的所有事件监听函数去掉
		 * @param id
		 */
		public static function removeAllListener(id:String):void
		{
			var teargetController:CommonEventController = getHandler(id) as CommonEventController;
			for(var singKye:String in teargetController.m_executers)
			{
				teargetController.removeEventListener(singKye,teargetController.eventExecute);
			}
			teargetController.m_executers ={};
		}
		
	}
}