package  eventengine
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.utils.Dictionary;
	
	/**
	 * 处理游戏中的消息，为特定类型的事件提供处理函数
	 * @author SDD
	 * 
	 */
	public class CommonEventController extends EventDispatcher
	{
		public var m_executers:Object={};								//记录handler
		
		public function CommonEventController(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		public function regEventHandler(eventType:String,handler:Function,params:* = null):void
		{
			if(!this.hasEventListener(eventType))
				this.addEventListener(eventType,eventExecute);
			
			var handlerObj:HandlerObject = new HandlerObject(handler,params);
			
			if(m_executers[eventType])
				m_executers[eventType][handler] = handlerObj;
			else
			{
				m_executers[eventType] = new Dictionary;
				m_executers[eventType][handler] = handlerObj;
			}
			
		}
		
		public function disRegEventHandler(eventType:String,handler:* = null):void
		{
			if(m_executers[eventType] == null)
				return;
			if(handler == null)
			{
				this.removeEventListener(eventType,eventExecute);
				delete m_executers[eventType];
			}
			else
			{
				delete m_executers[eventType][handler];
			}
		}
		
		public function getListenerCountOfType(eventType:String):int
		{
			if(m_executers[eventType] == null)
				return 0;
			var targetDic:Dictionary = m_executers[eventType];
			var retInfo:int = 0;
			for(var singleKey:* in targetDic)
			{
				retInfo++;
			}
			return retInfo;
		}
		
		public function eventExecute(event:Event):void
		{
			if(m_executers[event.type] == null)
				return;
			var handler:HandlerObject;
			
			var tempDictionary:Dictionary = new Dictionary;
			
			var handlerKey:*;
			
			for(handlerKey in m_executers[event.type])		
			{
				handler = m_executers[event.type][handlerKey] as HandlerObject;
				if(handler == null)
					continue;
				tempDictionary[handler] = 1;
			}
			
			var copuDictionary:Dictionary = new Dictionary();
			for(handlerKey in m_executers[event.type])
			{
				copuDictionary[handlerKey] = m_executers[event.type][handlerKey];
			}
			
			for(handlerKey in copuDictionary)
			{
				handler = copuDictionary[handlerKey] as HandlerObject;
				if(handler == null)
					continue;
				else
				{
					if(tempDictionary[handler] != 1)
					{
						continue;
					}
					if(handler is HandlerObject)									//如果注册的是方法
					{
						var tempFunc:Function = handler.handler;
						
						if(tempFunc != null)
						{
							if(handler.params != null)
							{
								tempFunc(event,handler.params);
							}
							else
							{
								tempFunc(event);
							}
						}
					}
				}
			}
			
			copuDictionary = null;
			
		}
		
	}
}